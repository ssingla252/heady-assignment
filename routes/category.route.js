var express = require('express');
var router = express.Router();
const asyncMiddleware = require('../middleware/async');

// Require the controllers WHICH WE DID NOT CREATE YET!!
var category_controller = require('../controllers/category.controller');


router.post('/', category_controller.createCategory);
router.get('/', category_controller.getCategories);

module.exports = router;