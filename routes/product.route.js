var express = require('express');
var router = express.Router();
const asyncMiddleware = require('../middleware/async');

// Require the controllers WHICH WE DID NOT CREATE YET!!
var product_controller = require('../controllers/product.controller');


router.post('/', asyncMiddleware(product_controller.createProduct));
router.get('/', asyncMiddleware(product_controller.getProducts));
router.put('/:productId', asyncMiddleware(product_controller.updateProduct));

module.exports = router;