var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name: {type: String, required: true, max: 100},
    fullname:{type: String, required: true, max: 100},
    parent:{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
    children:[{type: mongoose.Schema.Types.ObjectId, ref: 'Category'}]
 
});



// Export the model
module.exports = mongoose.model('Category', CategorySchema);