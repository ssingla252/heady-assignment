var Product = require('../models/product.model');
var Category = require('../models/category.model');

exports.createProduct = async (req, res) => {
        let categories = req.body.categories
        let product = await Product.find({ name: req.body.name })
        if (product.length != 0) {
            res.status(409).send({ err: "This product already exists", "product": product });
        }
        else if (categories == undefined || categories.length == 0) {
            res.status(409).send({ err: "Product should be linked to atleast one category" });
        }

        else {
            newProduct = new Product({ 'name': req.body.name, 'price': req.body.price, categories: req.body.categories })
            product = await newProduct.save()
            res.status(201).send(product);

        }
}

exports.getProducts = async (req, res) => {
        if (!req.query.categoryId.match(/^[0-9a-fA-F]{24}$/)) {
            res.status(409).send({err:"The categoryId is invalid"});
        }
        else {
            let category = await Category.findById(req.query.categoryId)
            let products = await Product.find().populate('categories', null, { fullname: new RegExp("^" + category.fullname) })
            products = products.filter((product) => {
                return product.categories.length;
            })
            res.status(200).send(products)
        }

}

exports.updateProduct= async (req,res) => {
    let productId=req.params.productId;
    let product=null;
    let name=req.body.name;
    let price=req.body.price
    console.log(productId)

    if (!productId.match(/^[0-9a-fA-F]{24}$/)) {
        res.status(409).send({err:"The productId is invalid"});
    }
    else
    {
        product=await Product.findById(productId)
        if(!product)
        {
            res.status(409).send({err:"The productId doesn't exist"});
        }
        else
        {
            if (name==undefined) name=product.name;
            if (price==undefined) price=product.price;
            product.name=name;
            product.price=price;
            product=await product.save()
            res.status(200).send(product);

        }
    }
}
