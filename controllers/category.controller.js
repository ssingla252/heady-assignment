var Category = require('../models/category.model');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.category_create = async (req, res) => {
    let category = await Category.find({ fullname: req.body.fullname })
    if (category.length != 0) {
        res.send({ staus: 0, msg: "This category also exists", category: category })
    }
    else {
        let levels = req.body.fullname.split('/')
        let name = levels[levels.length - 1]
        let categoryValidation = validFullname(req.body.fullname);
        let newCategory
        if (categoryValidation.valid != true) {
            status = 0;
            msg = categoryValidation.msg
            res.send({ staus: status, msg: msg })
        }
        if (req.body.fullname === "/Home") {
            newCategory = new Category({ name: name, fullname: req.body.fullname })
            category = await newCategory.save()
        }
        else {
            let parentFullname = getFullname(levels, levels.length - 1)
            let parent = await Category.find({ fullname: parentFullname })
            if (parent.length == 0) {
                res.send({ staus: 0, msg: "Parent doesn't exist" })
            }
            else {
                newCategory = new Category({ name: name, fullname: req.body.fullname, parent: parent[0].id })
                category = await newCategory.save()
                let children = parent[0].children
                children.push(category.id)
                parent[0].children = children
                parent = await parent[0].save()
            }

        }

        res.send({ staus: 1, msg: "Category created successfully", category: category })

    }

};
exports.createCategory = async (req, res) => {
    let parentId = req.body.parentId;
    let name = req.body.name;
    let fullname = ''
    let category, newCategory
    let parent = null
  
    if (parentId == undefined || parentId.trim() == "") {
        if (name == "Home") {
            category = await Category.findOne({ name: name })
            if (category) {
                res.status(409).send({err: "This category already exists", category: category });
            }
            else {
                fullname = `/${name}`
                newCategory = new Category({ name: name, fullname: fullname })
            }
        }
        else {
            res.status(409).send("Can't create a top category other than Home");
        }
    }
    else {
        if (!parentId.match(/^[0-9a-fA-F]{24}$/))
        {
            res.status(409).send("The parentId is invalid");
        }
        else
        {
        category = await Category.findOne({ name: name, "parent": parentId })
        if (category) {
            res.status(409).send( {msg: "This category already exists", category: category});
        }
        else {
            parent = await Category.findById(parentId)
            if (parent) {
                fullname = `${parent.fullname}/${name}`
                newCategory = new Category({ name: name, fullname: fullname, parent: parentId })

            }
            else {
                res.status(409).send({err:"The categoryId is invalid"});
            }
        }

    }

    }
    if (newCategory != undefined) {
        category = await newCategory.save()
        if (parent) {
            let children = parent.children
            if (children==undefined) children=[];
            children.push(category.id)
            parent.children = children
            parent = await parent.save()
        }
        res.status(201).send(category);
    }
    
  

}
exports.getCategories = async (req, res) => {
   
    let categories = await Category.find().populate('children', 'fullname')
    res.status(200).send(categories)

    
}

const getFullname = (levels, len) => {
    let fullname = ''
    for (let i = 1; i < len; i++) {
        fullname += `/${levels[i]}`
    }
    return (fullname)
}
const validFullname = (fullname) => {
    let valid = true
    let msg = ''
    let levels = fullname.split('/')
    if (!fullname.startsWith('/Home')) {
        valid = false;
        msg = 'All categories fullname should start with /Home';
    }
    else if (fullname.endsWith('/')) {
        valid = false;
        msg = 'Category Name should not end with /';
    }
    else if (levels.includes("", 1)) {
        valid = false;
        msg = 'Invalid Category';
    }
    else {
        valid = true
    }
    return ({ "valid": valid, "msg": msg })

}

