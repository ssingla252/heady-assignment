// app.js
"This is great"
require('express-async-errors')
const express = require('express');
const bodyParser = require('body-parser');
const winston=require('winston');
const product = require('./routes/product.route'); 

const category = require('./routes/category.route');
const error = require('./middleware/error');





const app = express();

winston.add(winston.transports.File,{filename:'logfile.log'})


// Set up mongoose connection
const mongoose = require('mongoose');
const dev_db_url = 'mongodb://localhost/heady';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/products', product);
app.use('/categories',category);
app.use(error);

const port = 1234;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

